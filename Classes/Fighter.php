<?php 

class Fighter {

    private $name; // string
   private  $hp;  // int
    private $init = 0; // int
 private $attack; // int

    public function __construct($name,$hp,$attack)
    {       
        $this->name = $name;
        $this->hp = $hp;
        $this->attack = $attack;

    }

    public function getHp()
    {
        return $this->hp;
    }
     
// setter
    public function replaceHp($hp){
        $this->hp = $hp ; 
    }

    public function getAttack()
    {
        return $this->attack;
    }

    public function getInit()
    {
        
        return $this->init;
    }


    
}

?>